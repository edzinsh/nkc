@extends('layouts.app')

@section('content')
<div class="container">
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Projekti</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('sound_format.index') }}">Skaņu formāti</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Ekrānu formāti</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Festivāli</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Studijas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Personas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Lomas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Konkursi</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Izmaksas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Izplatītāji</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Auditorijas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Valsts</a>
    <hr>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Projektu tipi</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Konkursu veidi</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Pieteikumi konkursiem</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Plānotās izmaksas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Citi finansējuma avoti</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Avotu Kategorijas</a>
    <a class="btn btn-home btn-primary btn-block" href="{{ route('projects.index') }}">Izplatīšanas tiesības</a>
    <hr>
    <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
        <button class="btn btn-home btn-primary btn-block">Iziet</button>
    </form>
</div>
@endsection
