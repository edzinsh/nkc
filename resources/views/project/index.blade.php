@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<div class="card bg-primary">
				<div class="card-header text-white">
					<span class="card-justify-middle float-left">Projekti</span> <a class="btn btn-secondary float-right" href="{{ route('projects.create') }}">Pievienot jaunu</a>
				</div>
				<div class="card-body bg-white">
					<table class="table">
						<thead>
							<tr>
								<th>id</th>
								<th>Nosaukums LV</th>
								<th>Nosaukums EN</th>
								<th>Nosaukums Oriģinālvalodā</th>
								<th>Sinopse LV</th>
								<th>Sinopse EN</th>
								<th>Projekta Veids</th>
								<th>Metrāža</th>
								<th>Garums (MIN)</th>
								<th>Īstenošanas laiks (NO)</th>
								<th>Īstenošanas laiks (LĪDZ)</th>
								<th></th>
							</tr>
							<tr>
								<td>Kaut kas</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection