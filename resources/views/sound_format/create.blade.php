@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<div class="card bg-primary">
				<div class="card-header text-white">
					<span>Pievienot jaunu</span>
				</div>
				<div class="card-body bg-white">
					<form method="post" action="{{ route('sound_format.store') }}">
						@csrf
						<div class="form-group">
							<input class="form-control" type="text" name="title" placeholder="Skaņas formāta nosaukums" autofocus="" required="">
						</div>
						<button class="btn btn-success" type="submit">Izveidot</button>
						<a href="{{ route('sound_format.index') }}" class="btn btn-primary">Atgriezties atpakaļ</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection