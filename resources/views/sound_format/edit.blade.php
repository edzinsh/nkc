@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<div class="card bg-primary">
				<div class="card-header text-white">
					<span>Labot ierakstu - {{ $sound_format['id'] }} ( {{ $sound_format['title'] }} )</span>
				</div>
				<div class="card-body bg-white">
					<form method="post" action="{{ route('sound_format.update', $sound_format['id']) }}">
						@csrf
						{{ method_field('PUT') }}
						<div class="form-group">
							<input class="form-control" type="text" name="title" placeholder="Skaņas formāta nosaukums" @if ($sound_format['title']) value="{{ $sound_format['title'] }}" @endif autofocus="" required="">
						</div>
						<button class="btn btn-success" type="submit">Labot</button>
						<a href="{{ route('sound_format.index') }}" class="btn btn-primary">Atgriezties atpakaļ</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection