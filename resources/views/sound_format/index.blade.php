@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			@if (session('status'))
		    	<div class="alert alert-{{ session('status') }}">
		    		{{ session('message') }}
		    	</div>
		    @endif
			<div class="card bg-primary">
				<div class="card-header text-white">
					<span class="card-justify-middle float-left">Skaņu formāti</span> <a class="btn btn-secondary float-right" href="{{ route('sound_format.create') }}">Pievienot jaunu</a>
				</div>
				<table class="table bg-white">
					<thead>
						<tr>
							<th>id</th>
							<th>Nosaukums</th>
							<th></th>
						</tr>
						@foreach ($sound_formats as $sound_format)
							<tr class="clickableRow" href="{{ route('sound_format.edit', $sound_format['id']) }}">
								<td>{{ $sound_format['id'] }}</td>
								<td>{{ $sound_format['title'] }}</td>
								<td class="delete">
									<form method="post" action="{{ route('sound_format.destroy', $sound_format['id']) }}">
										@csrf
										{{ method_field('DELETE') }}
										<button class="btn float-right">
											<svg class="bi bi-x" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											  <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 010 .708l-7 7a.5.5 0 01-.708-.708l7-7a.5.5 0 01.708 0z" clip-rule="evenodd"/>
											  <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 000 .708l7 7a.5.5 0 00.708-.708l-7-7a.5.5 0 00-.708 0z" clip-rule="evenodd"/>
											</svg>
										</button>
									</form>
								</td>
							</tr>
						@endforeach
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection