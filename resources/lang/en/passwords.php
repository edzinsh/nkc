<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Parole veiksmīgi nomainīta!',
    'sent' => 'Esam aizsūtījuši jums saiti uz parole nomaiņu!',
    'throttled' => 'Lūdzu uzgaidiet pirms mēģināt vēlreiz.',
    'token' => 'This password reset token is invalid.',
    'user' => "Nevaram atrast lietotāju ar šādu e-pastu.",

];
