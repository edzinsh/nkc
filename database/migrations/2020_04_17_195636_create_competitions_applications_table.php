<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionsApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions_applications', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id');
            $table->dateTime('competition_date', 0);
            $table->double('requested_funding', 4, 2);
            $table->double('allocated_funding', 4, 2);
            $table->dateTime('payout_date', 0);
            $table->dateTime('date_of_contract', 0);
            $table->dateTime('date_of_commission_decision', 0);
            // Vadītāja lēmuma datums <---- Šeit
            $table->dateTime('commision_decision', 0);
            // Vadītāja lēmums <---- Šeit
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions_applications');
    }
}
