<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributionRightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribution_rights', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id');
            $table->integer('distributor_id');
            // Iespēja saistīt ar vairākiem kanāliem (viens pret daudziem)
            $table->double('receipt', 4, 2);
            // Iespēja pievienot vairākas valstis
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribution_rights');
    }
}
