<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title_lv');
            $table->string('title_en');
            $table->string('title_original');
            $table->text('synopsis_lv');
            $table->text('synopsis_en');
            $table->integer('project_type_id');
            $table->string('footage');
            $table->integer('film_length');
            $table->dateTime('implementation time', 0);
            $table->dateTime('scheduled_premiere_time', 0);
            $table->integer('festival_id');
            $table->dateTime('actual_premiere_date', 0);
            $table->dateTime('international_premiere_date', 0);
            $table->integer('registration_number');
            $table->integer('produce');
            $table->dateTime('end_date', 0);
            $table->text('comment');
            $table->integer('colored');
            $table->integer('screen_format');
            $table->integer('sound');
            $table->string('homepage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
