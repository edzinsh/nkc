<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sound_format;
use Session;

class SoundFormatController extends Controller
{
    public function index(Request $request)
    {
    	$sound_formats = Sound_format::all();
    	return view('sound_format.index')->with(compact('sound_formats'));
    }

    public function edit($id)
    {
    	$sound_format = Sound_format::findOrFail($id);
    	return view('sound_format.edit')->with(compact('sound_format'));
    }

    public function update(Request $request, $id)
    {
    	$sound_format = Sound_format::findOrFail($id);
    	$sound_format->title = $request->input('title');
    	$sound_format->timestamps = false;
    	if ($sound_format->save())
    	{
    		return redirect(route('sound_format.index'))->with(['status' => 'success', 'message' => 'Ieraksts veiksmīgi labots!']);
    	} else {
    		return redirect(route('sound_format.index'))->with(['status' => 'danger', 'message' => 'Notika kļūda!']);
    	}
    }

    public function create()
    {
    	return view('sound_format.create');
    }

    public function store(Request $request)
    {
    	$sound_format = new Sound_format();
    	$sound_format->title = $request->input('title');
    	$sound_format->timestamps = false;
    	if ($sound_format->save())
    	{
    		return redirect(route('sound_format.index'))->with(['status' => 'success', 'message' => 'Veiksmīgi izveidojis ierakstu!']);
    	} else {
    		return redirect(route('sound_format.index'))->with(['status' => 'danger', 'message' => 'Notika kļūda!']);
    	}
    }

    public function destroy($id)
    {
    	$sound_format = Sound_format::findOrFail($id);
    	if ($sound_format->delete())
    	{
    		return redirect(route('sound_format.index'))->with(['status' => 'success', 'message' => 'Veiksmīgi dzēsts ieraksts!']);
    	} else {
    		return redirect(route('sound_format.index'))->with(['status' => 'danger', 'message' => 'Notika kļūda!']);
    	}
    }
}
